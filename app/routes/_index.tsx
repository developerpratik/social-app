import React, {useEffect, useState} from "react";
import {ethers} from "ethers";
import todolist from '../solidity/todolist.json'
const index = () => {
    const [error, setError] = useState('')
    const [wallet, setWallet] = useState('')
    const [todos, setTodos] = useState([]);
    const [task, setTask] = useState("")
    useEffect(() => {
        const getProvider = async () => {
            // @ts-ignore
            const account = await window?.ethereum.request({method: "eth_accounts"})
            if (account.length === 0) {
                alert('please first  install metamask');
                return setError('please first  install metamask');
            }
            setWallet(account[0]);
        }
        // @ts-ignore
        getProvider().then().catch((e) => console.error(e))
    }, [])


    const getTodos = async () => {
        // @ts-ignore
        const provider = new ethers.BrowserProvider(window.ethereum);
        const signer = await provider.getSigner();
        const contract = new ethers.Contract(wallet, todolist.abi, signer);
        const todos = await contract.getTodos();
        setTodos(todos);
    };
    const addTodo = async () => {
        // @ts-ignore
        const provider = new ethers.BrowserProvider(window.ethereum);
        const signer =  await provider.getSigner();
        const contract = new ethers.Contract(wallet, todolist.abi, signer);
        await contract.addTodo(task);
        getTodos();
    };
    const removeTodo = async (index: number) => {
        // @ts-ignore
        const provider = new ethers.BrowserProvider(window?.ethereum);
        const signer =await provider.getSigner();
        const contract = new ethers.Contract(wallet, todolist.abi, signer);
        await contract.removeTodo(index);
        getTodos();
    };

    return (<div className="flex flex-col items-center justify-center  h-[100vh]  bg-slate-800">

        {error && <div className="flex flex-col space-y-3 m-auto text-white">
            {error}
        </div>
        }
        {wallet &&
            <div className="flex flex-col space-y-3"><input
                id="task"
                name="task"
                type="text"
                value={task}
                onChange={(e) => setTask(e.target.value)}
                autoComplete="off"
                required
                className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
            />
                <button
                    type="button"
                    onClick={addTodo}
                    className="flex w-full justify-center rounded-md bg-indigo-600 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                >
                    add todo
                </button>
            </div>
        }

    </div>)

}

export default index;