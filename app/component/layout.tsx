// app/components/layout.tsx
export function Layout({ children }: { children: React.ReactNode }) {
    return <div className="h-[100vh] w-full  relative flex overflow-auto overflow-x-hidden">{children}</div>
  }