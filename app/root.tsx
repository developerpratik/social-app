import type { LinksFunction } from "@remix-run/node";
import {
  Links,
  LiveReload,
  Meta,
  Outlet,
  Scripts,
  ScrollRestoration,
} from "@remix-run/react";

import styles from "../app/styles/app.css";
export const links: LinksFunction = () => [
  ...([{ rel: "stylesheet", href: styles },{rel:"preconnect", href:"https://fonts.googleapis.com"},  {rel:"preconnect", href:"https://fonts.googleapis.com"},{rel:"stylesheet", href:"https://fonts.googleapis.com/css2?family=Alumni+Sans+Collegiate+One:ital@0;1&family=Playfair+Display:wght@400;500&family=Roboto:ital,wght@0,300;0,400;1,400&family=Young+Serif&display=swap"}]),
];

const App = () => {
  return (
    <html lang="en">
      <head>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <Meta />
        <Links />
      </head>
      <body>
        <Outlet />
        <ScrollRestoration />
        <Scripts />
        <LiveReload />
      </body>
    </html>
  );
}


export default App;